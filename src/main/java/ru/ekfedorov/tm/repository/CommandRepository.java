package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.ICommandRepository;
import ru.ekfedorov.tm.command.AbstractCommand;

import java.util.*;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getCommandArgs() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String arg = command.arg();
            if (isEmpty(arg)) continue;
            result.add(arg);
        }
        return result;
    }

    @Override
    public AbstractCommand getCommandByArg(final String arg) {
        return commands.get(arg);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commands.get(name);
    }

    @Override
    public Collection<String> getCommandName() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.name();
            if (isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}
