package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public Task bindTaskByProjectId(
            final String userId, final String projectId, final String taskId
    ) {
        final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(
            final String userId, final String projectId
    ) {
        if (isEmpty(projectId)) return null;
        final List<Task> list1 = new ArrayList<>();
        for (Task task : list) {
            if (projectId.equals(task.getProjectId()) &&
                    userId.equals(task.getUserId())) list1.add(task);
        }
        return list1;
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        final List<Task> list2 = new ArrayList<>();
        for (final Task task : list) {
            if (userId.equals(task.getUserId())) list2.add(task);
        }
        if (index >= list2.size()) return null;
        return list2.get(index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        for (final Task task : list) {
            if (name.equals(task.getName()) &&
                    userId.equals(task.getUserId())) return task;
        }
        return null;
    }

    @Override
    public void removeAllByProjectId(
            final String userId, final String projectId
    ) {
        if (isEmpty(projectId)) return;
        list.removeIf(
                task -> projectId.equals(task.getProjectId()) &&
                        userId.equals(task.getUserId())
        );
    }

    @Override
    public Task removeOneByIndex(
            final String userId, final Integer index
    ) {
        final Task task = findOneByIndex(userId, index);
        if (task == null) return null;
        return remove(userId, task);
    }

    @Override
    public Task removeOneByName(
            final String userId, final String name
    ) {
        final Task task = findOneByName(userId, name);
        if (task == null) return null;
        return remove(userId, task);
    }

    @Override
    public Task unbindTaskFromProjectId(
            final String userId, final String taskId
    ) {
        final Task task = findOneById(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}
