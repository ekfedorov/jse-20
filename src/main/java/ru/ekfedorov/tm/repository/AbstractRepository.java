package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public E add(final E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void clear(final String userId) {
        for (int i = 0; i < list.size(); i++) {
            remove(userId, list.get(i));
        }
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> list2 = new ArrayList<>();
        for (E entity : list) {
            if (userId.equals(entity.getUserId())) list2.add(entity);
        }
        return list2;
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        final List<E> list2 = new ArrayList<>();
        for (E entity : list) {
            if (userId.equals(entity.getUserId())) list2.add(entity);
        }
        list2.sort(comparator);
        return list2;
    }

    @Override
    public E findOneById(final String userId, final String id) {
        for (E entity : list) {
            if (id.equals(entity.getId()) &&
                    userId.equals(entity.getUserId())) return entity;
        }
        return null;
    }

    @Override
    public E remove(final String userId, final E entity) {
        if (userId.equals(entity.getUserId())) {
            list.remove(entity);
            return entity;
        }
        return null;
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        final E entity = findOneById(userId, id);
        if (entity == null) return null;
        return remove(userId, entity);
    }

}
