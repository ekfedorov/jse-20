package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.model.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class UserRepository implements IUserRepository {

    protected final List<User> list = new ArrayList<>();

    @Override
    public User add(final User entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void clear() {
        for (int i = 0; i < list.size(); i++) {
            User user = list.get(i);
            if (user.getRole() == Role.USER) remove(user);
        }
    }

    @Override
    public List<User> findAll() {
        return list;
    }

    @Override
    public List<User> findAll(final Comparator<User> comparator) {
        final List<User> entities = new ArrayList<>(list);
        entities.sort(comparator);
        return entities;
    }

    @Override
    public User findByLogin(final String login) {
        for (User user : list) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findOneById(final String id) {
        for (User entity : list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public User remove(final User entity) {
        list.remove(entity);
        return entity;
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return remove(user);
    }

    @Override
    public User removeOneById(final String id) {
        final User entity = findOneById(id);
        if (entity == null) return null;
        return remove(entity);
    }

}
