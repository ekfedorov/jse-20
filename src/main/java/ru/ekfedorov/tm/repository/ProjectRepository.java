package ru.ekfedorov.tm.repository;

import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        final List<Project> list2 = new ArrayList<>();
        for (final Project project : list) {
            if (userId.equals(project.getUserId())) list2.add(project);
        }
        if (index >= list2.size()) return null;
        return list2.get(index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        for (final Project project : list) {
            if (name.equals(project.getName()) &&
                    userId.equals(project.getUserId())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        return remove(userId, project);
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        return remove(userId, project);
    }

}
