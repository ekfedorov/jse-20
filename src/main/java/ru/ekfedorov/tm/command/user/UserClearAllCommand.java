package ru.ekfedorov.tm.command.user;

import ru.ekfedorov.tm.command.AbstractUserCommand;
import ru.ekfedorov.tm.enumerated.Role;

public class UserClearAllCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear all users.";
    }

    @Override
    public void execute() throws Exception {
        final Role role = serviceLocator.getAuthService().getUser().getRole();
        System.out.println("[CLEAR ALL USERS]");
        serviceLocator.getUserService().clear(role);
    }

    @Override
    public String name() {
        return "user-clear-all";
    }

}
