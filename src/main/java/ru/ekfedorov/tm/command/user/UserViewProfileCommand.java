package ru.ekfedorov.tm.command.user;

import ru.ekfedorov.tm.command.AbstractUserCommand;
import ru.ekfedorov.tm.model.User;

public class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "View my profile.";
    }

    @Override
    public void execute() throws Exception {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("USER ID: " + user.getId());
    }

    @Override
    public String name() {
        return "view-profile";
    }

}
