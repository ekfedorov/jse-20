package ru.ekfedorov.tm.command.task;

import ru.ekfedorov.tm.api.service.IProjectTaskService;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

public class TaskBindByProjectIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task by project.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[BIND TASK BY PROJECT]");
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final IProjectTaskService projectTaskService = serviceLocator.getProjectTaskService();
        final Task task = projectTaskService.bindTaskByProject(userId, projectId, taskId);
        if (task == null) throw new NullTaskException();
    }

    @Override
    public String name() {
        return "bind-task-by-project";
    }

}
