package ru.ekfedorov.tm.command.project;

import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        if (project == null) throw new NullProjectException();
        showProject(project);
        System.out.println();
    }

    @Override
    public String name() {
        return "project-view-by-id";
    }

}
