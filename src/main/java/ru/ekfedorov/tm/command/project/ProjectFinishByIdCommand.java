package ru.ekfedorov.tm.command.project;

import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.util.TerminalUtil;

public class ProjectFinishByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Finish project by id.";
    }

    @Override
    public void execute() throws Exception {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishProjectById(userId, id);
        if (project == null) throw new NullProjectException();
    }

    @Override
    public String name() {
        return "finish-project-by-id";
    }

}
