package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.repository.IUserRepository;
import ru.ekfedorov.tm.api.service.IUserService;
import ru.ekfedorov.tm.exception.system.AccessDeniedException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.User;
import ru.ekfedorov.tm.enumerated.Role;
import ru.ekfedorov.tm.exception.system.LoginExistException;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.util.HashUtil;

import java.util.Comparator;
import java.util.List;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User add(final User entity) throws Exception {
        if (entity == null) throw new NullObjectException();
        return userRepository.add(entity);
    }

    @Override
    public void clear(final Role role) throws Exception {
        if (role != Role.ADMIN) throw new AccessDeniedException();
        userRepository.clear();
    }

    @Override
    public User create(final String login, final String password) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return add(user);
    }

    @Override
    public void create(final String login, final String password, final String email) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (isEmpty(email)) throw new EmailIsEmptyException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        add(user);
    }

    @Override
    public void create(final String login, final String password, final Role role) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        if (role == null) throw new RoleIsEmptyException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        add(user);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findAll(final Comparator<User> comparator) throws Exception {
        if (comparator == null) throw new NullComparatorException();
        return userRepository.findAll(comparator);
    }

    @Override
    public User findByLogin(final String login) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User findOneById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return userRepository.findOneById(id);
    }

    @Override
    public boolean isLoginExist(final String login) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        return userRepository.isLoginExist(login);
    }

    @Override
    public User remove(final User entity) throws Exception {
        if (entity == null) throw new NullObjectException();
        return userRepository.remove(entity);
    }

    @Override
    public User removeByLogin(final String login) throws Exception {
        if (isEmpty(login)) throw new LoginIsEmptyException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User removeOneById(final String id) throws Exception {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return userRepository.removeOneById(id);
    }

    @Override
    public void setPassword(final String userId, final String password) throws Exception {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        if (isEmpty(password)) throw new PasswordIsEmptyException();
        final User user = findOneById(userId);
        if (user == null) return;
        final String hash = HashUtil.salt(password);
        if (hash == null) return;
        user.setPasswordHash(hash);
    }

    @Override
    public void userUpdate(
            final String userId,
            final String firstName,
            final String lastName,
            final String middleName
    ) throws Exception {
        if (isEmpty(userId)) throw new IdIsEmptyException();
        final User user = findOneById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
    }

}
