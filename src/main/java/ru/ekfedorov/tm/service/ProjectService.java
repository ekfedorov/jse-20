package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.empty.UserIdIsEmptyException;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.empty.DescriptionIsEmptyException;
import ru.ekfedorov.tm.exception.empty.IdIsEmptyException;
import ru.ekfedorov.tm.exception.empty.NameIsEmptyException;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.model.Project;

import static ru.ekfedorov.tm.util.ValidateUtil.*;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final String userId, final String name, final String description) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project changeProjectStatusById(
            final String userId, final String id, final Status status
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(
            final String userId, final Integer index, final Status status
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(
            final String userId, final String name, final Status status
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new NullProjectException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public Project finishProjectById(final String userId, final String id) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final String userId, final Integer index) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project finishProjectByName(final String userId, final String name) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public Project removeOneByName(final String userId, final String name) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public Project startProjectById(final String userId, final String id) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final String userId, final Integer index) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String userId, final String name) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Project project = findOneByName(userId, name);
        if (project == null) throw new NullProjectException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project updateProjectById(
            final String userId, final String id, final String name, final String description
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null) throw new NullProjectException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(
            final String userId, final Integer index, final String name, final String description
    ) throws Exception {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException(index);
        if (isEmpty(name)) throw new NameIsEmptyException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) throw new NullProjectException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
