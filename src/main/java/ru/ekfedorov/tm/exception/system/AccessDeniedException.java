package ru.ekfedorov.tm.exception.system;


import ru.ekfedorov.tm.exception.AbstractException;

public final class AccessDeniedException extends AbstractException {

    public AccessDeniedException() throws Exception {
        super("Access denied...");
    }
}
