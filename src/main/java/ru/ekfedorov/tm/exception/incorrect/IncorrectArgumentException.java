package ru.ekfedorov.tm.exception.incorrect;

import ru.ekfedorov.tm.exception.AbstractException;

public final class IncorrectArgumentException extends AbstractException {

    public IncorrectArgumentException(String arg) throws Exception {
        super("Error! Argument ``" + arg + "`` not found...");
    }

}
