package ru.ekfedorov.tm.model;

import ru.ekfedorov.tm.api.entity.IWBS;
import ru.ekfedorov.tm.enumerated.Status;

import java.util.Date;

public final class Task extends AbstractEntity implements IWBS {

    private String projectId;

    public Task() {
    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

}
