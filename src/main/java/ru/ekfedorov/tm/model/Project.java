package ru.ekfedorov.tm.model;

import ru.ekfedorov.tm.api.entity.IWBS;
import ru.ekfedorov.tm.enumerated.Status;

import java.util.Date;

public final class Project extends AbstractEntity implements IWBS {

    public Project() {
    }

    public Project(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

}
