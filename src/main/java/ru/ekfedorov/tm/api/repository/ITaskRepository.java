package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task bindTaskByProjectId(final String userId, String projectId, String taskId);

    List<Task> findAllByProjectId(final String userId, String projectId);

    Task findOneByIndex(final String userId, Integer index);

    Task findOneByName(final String userId, String name);

    void removeAllByProjectId(final String userId, String projectId);

    Task removeOneByIndex(final String userId, Integer index);

    Task removeOneByName(final String userId, String name);

    Task unbindTaskFromProjectId(final String userId, String taskId);

}