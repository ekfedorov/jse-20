package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project findOneByIndex(String userId, Integer index);

    Project findOneByName(String userId, String name);

    Project removeOneByIndex(String userId, Integer index);

    Project removeOneByName(String userId, String name);

}
