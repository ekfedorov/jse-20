package ru.ekfedorov.tm.api.repository;

import ru.ekfedorov.tm.api.IRepository;
import ru.ekfedorov.tm.model.User;

import java.util.Comparator;
import java.util.List;

public interface IUserRepository {

    User add(User entity);

    void clear();

    List<User> findAll();

    List<User> findAll(Comparator<User> comparator);

    User findByLogin(String login);

    User findOneById(String id);

    Boolean isLoginExist(String login);

    User remove(User entity);

    User removeByLogin(String login);

    User removeOneById(String id);

}
