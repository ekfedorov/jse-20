package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project add(final String userId, String name, String description) throws Exception;

    Project changeProjectStatusById(final String userId, String id, Status status) throws Exception;

    Project changeProjectStatusByIndex(final String userId, Integer index, Status status) throws Exception;

    Project changeProjectStatusByName(final String userId, String name, Status status) throws Exception;

    Project findOneByIndex(final String userId, Integer index) throws Exception;

    Project findOneByName(final String userId, String name) throws Exception;

    Project finishProjectById(final String userId, String id) throws Exception;

    Project finishProjectByIndex(final String userId, Integer index) throws Exception;

    Project finishProjectByName(final String userId, String name) throws Exception;

    Project removeOneByIndex(final String userId, Integer index) throws Exception;

    Project removeOneByName(final String userId, String name) throws Exception;

    Project startProjectById(final String userId, String id) throws Exception;

    Project startProjectByIndex(final String userId, Integer index) throws Exception;

    Project startProjectByName(final String userId, String name) throws Exception;

    Project updateProjectById(final String userId, String id, String name, String description) throws Exception;

    Project updateProjectByIndex(final String userId, Integer index, String name, String description) throws Exception;

}
