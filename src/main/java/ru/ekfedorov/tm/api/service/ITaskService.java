package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.api.IService;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.model.Task;

public interface ITaskService extends IService<Task> {

    Task add(final String userId, String name, String description) throws Exception;

    Task changeTaskStatusById(final String userId, String id, Status status) throws Exception;

    Task changeTaskStatusByIndex(final String userId, Integer index, Status status) throws Exception;

    Task changeTaskStatusByName(final String userId, String name, Status status) throws Exception;

    Task findOneByIndex(final String userId, Integer index) throws Exception;

    Task findOneByName(final String userId, String name) throws Exception;

    Task finishTaskById(final String userId, String id) throws Exception;

    Task finishTaskByIndex(final String userId, Integer index) throws Exception;

    Task finishTaskByName(final String userId, String name) throws Exception;

    Task removeOneByIndex(final String userId, Integer index) throws Exception;

    Task removeOneByName(final String userId, String name) throws Exception;

    Task startTaskById(final String userId, String id) throws Exception;

    Task startTaskByIndex(final String userId, Integer index) throws Exception;

    Task startTaskByName(final String userId, String name) throws Exception;

    Task updateTaskById(final String userId, String id, String name, String description) throws Exception;

    Task updateTaskByIndex(final String userId, Integer index, String name, String description) throws Exception;

}
