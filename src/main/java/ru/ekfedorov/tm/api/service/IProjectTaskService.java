package ru.ekfedorov.tm.api.service;

import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskByProject(final String userId, String projectId, String taskId) throws Exception;

    List<Task> findAllByProjectId(final String userId, String projectId) throws Exception;

    Project removeProjectById(final String userId, String projectId) throws Exception;

    Task unbindTaskFromProject(final String userId, String taskId) throws Exception;

}
