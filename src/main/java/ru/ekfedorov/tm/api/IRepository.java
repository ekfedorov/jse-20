package ru.ekfedorov.tm.api;

import ru.ekfedorov.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    E add(E entity) throws Exception;

    void clear(String userId);

    List<E> findAll(String userId, Comparator<E> comparator) throws Exception;

    List<E> findAll(String userId);

    E findOneById(String userId, String id) throws Exception;

    E remove(String userId, E entity) throws Exception;

    E removeOneById(String userId, String id) throws Exception;

}
